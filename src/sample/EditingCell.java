package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import sample.datamodel.Contact;

class EditingCell extends TableCell<Contact, String>
{
    private TextField textField;

    public EditingCell()
    {
    }

    @Override
    public void startEdit()
    {
        if (!isEditable() || !getTableView().isEditable()
                || !getTableColumn().isEditable())
        {
            return;
        }
        super.startEdit();

        if (isEditing())
        {
            if (textField == null)
            {
                createTextField();
            }
            setText(null);
            setGraphic(textField);
            textField.selectAll();
            // requesting focus so that key input can immediately go into
            // the TextField (see RT-28132)
            textField.requestFocus();
        }
    }

    @Override
    public void cancelEdit()
    {
        super.cancelEdit();

        setText((String) getItem());
        setGraphic(null);
    }

    @Override
    public void updateItem(String item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty)
        {
            setText(null);
            setGraphic(null);
        } else
        {
            if (isEditing())
            {
                if (textField != null)
                {
                    textField.setText(getString());
                }
                setText(null);
                setGraphic(textField);
            } else
            {
                setText(getString());
                setGraphic(null);
            }
        }
    }

    private void createTextField()
    {
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0,
                                Boolean arg1, Boolean arg2)
            {
                if (!arg2)
                {
                    commitEdit(textField.getText());
                }
            }
        });

        // Use onAction here rather than onKeyReleased (with check for
        // Enter), as otherwise we encounter RT-34685
        textField.setOnAction(t -> {
            commitEdit(textField.getText());
            t.consume();
        });
        textField.setOnKeyReleased(t -> {
            if (t.getCode() == KeyCode.ESCAPE)
            {
                cancelEdit();
                t.consume();
            }
        });

        textField.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent t)
            {
                if ((t.getCode() == KeyCode.UP) || (t.getCode() == KeyCode.DOWN)
                        || (t.getCode() == KeyCode.LEFT)
                        || (t.getCode() == KeyCode.RIGHT))
                {
                    // Commit the current text
                    commitEdit(textField.getText());

                    // Let's move out simulating a key press in this Cell
                    KeyEvent event = new KeyEvent(t.getSource(), t.getTarget(),
                            KeyEvent.KEY_PRESSED, "", "", t.getCode(), false, false,
                            false, false);
                    EditingCell.this.fireEvent(event);
                }
            }
        });
    }

    private String getString()
    {
        return getItem() == null ? "" : getItem().toString();
    }

}