package sample;

import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import sample.datamodel.Contact;

public class DialogController {
    @FXML
    private TextField firstnameField;
    @FXML
    private TextField lastnameField;
    @FXML
    private TextField phonenumberField;
    @FXML
    private TextField notesField;

    private static BooleanBinding inputsFull ;

    public static BooleanBinding inputsFullBinding() {
        return inputsFull ;
    }

    public final boolean getInputsFull() {
        return inputsFull.get();
    }
    public void initialize() {
        inputsFull = new BooleanBinding() {
            {
                bind(firstnameField.textProperty(),
                        lastnameField.textProperty(),
                        phonenumberField.textProperty());
            }
            @Override
            protected boolean computeValue() {
                return ! (firstnameField.getText().trim().isEmpty()
                        || lastnameField.getText().trim().isEmpty()
                        || phonenumberField.getText().trim().isEmpty());
            }
        };
    }

    public void initData(Contact contact){
        if(contact==null) {
            return;
        }
        firstnameField.setText(contact.getFirstName());
        lastnameField.setText(contact.getLastName());
        phonenumberField.setText(contact.getPhoneNumber());
        notesField.setText(contact.getNotes());
    }

    public Contact addContact(){
        String firstName = firstnameField.getText();
        String lastName = lastnameField.getText();
        String phoneNumber = phonenumberField.getText();
        String notes = notesField.getText();
        Contact newContact = new Contact(firstName, lastName, phoneNumber, notes);
        return newContact;
    }

    public void updateContact(Contact contact){
        contact.setFirstName(firstnameField.getText());
        contact.setLastName(lastnameField.getText());
        contact.setPhoneNumber(phonenumberField.getText());
        contact.setNotes(notesField.getText());
    }

}
