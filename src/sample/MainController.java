package sample;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import sample.datamodel.Contact;
import sample.datamodel.ContactData;

import java.io.IOException;
import java.util.Optional;

public class MainController {
    @FXML
    BorderPane mainBorderPane;
    @FXML
    private TableView<Contact> tableView;
    @FXML
    private TableColumn firstNameCol;
    @FXML
    private TableColumn lastNameCol;
    @FXML
    private TableColumn phoneNumberCol;
    @FXML
    private TableColumn notesCol;
    private ContactData contactData;

    public void initialize() {
        contactData = new ContactData();
        contactData.loadContacts();
        tableView.setItems(contactData.getContacts());
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.setEditable(true);

        Callback<TableColumn, TableCell> cellFactory =
                new Callback<TableColumn, TableCell>() {
                    public TableCell call(TableColumn p) {
                        return new EditingCell();
                    }
                };

        // See https://stackoverflow.com/questions/15892630/has-anyone-figured-out-how-to-make-a-javafx-tableview-act-like-a-jtable

        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Contact, String>("firstName"));
        firstNameCol.setCellFactory(cellFactory);
        firstNameCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Contact, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Contact, String> t) {
                        ((Contact) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setFirstName(t.getNewValue());
                        contactData.saveContacts();
                    }
                }
        );
        lastNameCol.setCellValueFactory(
                new PropertyValueFactory<Contact, String>("lastName"));
        lastNameCol.setCellFactory(cellFactory);
        lastNameCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Contact, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Contact, String> t) {
                        ((Contact) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setLastName(t.getNewValue());
                        contactData.saveContacts();
                    }
                }
        );
        phoneNumberCol.setCellValueFactory(
                new PropertyValueFactory<Contact, String>("phoneNumber"));
        phoneNumberCol.setCellFactory(cellFactory);
        phoneNumberCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Contact, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Contact, String> t) {
                        ((Contact) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setPhoneNumber(t.getNewValue());
                        contactData.saveContacts();
                    }
                }
        );
        notesCol.setCellValueFactory(
                new PropertyValueFactory<Contact, String>("notes"));
        notesCol.setCellFactory(cellFactory);

        notesCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Contact, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Contact, String> t) {
                        ((Contact) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setNotes(t.getNewValue());
                        contactData.saveContacts();
                    }
                }
        );
    }
    @FXML
    public void showAddItemDialog(){
        handleDialog("Add contact", "Add information for the new contact", null);
    }

    @FXML
    public void showEditItemDialog(){
        Contact selectedContact = tableView.getSelectionModel().getSelectedItem();
        if(selectedContact==null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Select a contact");
            alert.setHeaderText(null);
            alert.setContentText("Please select a contact to edit");
            alert.showAndWait();
            return;
        }
        handleDialog("Edit contact", "Edit information for the contact", selectedContact);
    }

    public void handleDialog(String title, String headerText, Contact selectedContact){
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainBorderPane.getScene().getWindow());
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("contactDialog.fxml"));
        try{
            dialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            System.out.println("Error loading dialog");
            e.printStackTrace();
            return;
        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().lookupButton(ButtonType.OK)
                .disableProperty()
                .bind(DialogController.inputsFullBinding().not());
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        DialogController controller = fxmlLoader.getController();

        controller.initData(selectedContact);

        Optional<ButtonType> result = dialog.showAndWait();
        if(result.isPresent() && result.get() == ButtonType.OK){
            controller = fxmlLoader.getController();
            if(selectedContact==null){
                Contact newContact = controller.addContact();
                contactData.addContact(newContact);
            } else{
                controller.updateContact(selectedContact);
            }
            contactData.saveContacts();
        }
    }

    @FXML
    public void showDeleteItemDialog(){
        Contact selectedContact = tableView.getSelectionModel().getSelectedItem();
        if (selectedContact != null){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete contact");
            alert.setHeaderText("Delete contact " + selectedContact.getFirstName() + " " + selectedContact.getLastName());
            alert.setContentText("Are you sure? Press OK to confirm or Cancel to cancel");
            Optional<ButtonType> result = alert.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK) {
                contactData.deleteContact(selectedContact);
                contactData.saveContacts();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Select a contact");
            alert.setHeaderText(null);
            alert.setContentText("Please select a contact to delete");
            alert.showAndWait();
        }
    }
    @FXML
    public void exitApplication() {
        Platform.exit();
    }

}
