package sample.datamodel;

import javafx.beans.property.SimpleStringProperty;

import java.util.LinkedHashMap;

public class Contact {
    private SimpleStringProperty firstName = new SimpleStringProperty("");
    private SimpleStringProperty lastName = new SimpleStringProperty("");
    private SimpleStringProperty phoneNumber = new SimpleStringProperty("");
    private SimpleStringProperty notes = new SimpleStringProperty("");

    public Contact() {
        this("","","","");
    }

    public Contact(String firstName, String lastName, String phoneNumber, String notes) {
        setFirstName(firstName);
        setLastName(lastName);
        setPhoneNumber(phoneNumber);
        setNotes(notes);
    }

    public Contact(LinkedHashMap<String, Object> contactData){
        setFirstName(contactData.get("firstName").toString());
        setLastName(contactData.get("lastName").toString());
        setPhoneNumber(contactData.get("phoneNumber").toString());
        setNotes(contactData.get("notes").toString());
    }

    public LinkedHashMap<String, Object> getContactData(){
        LinkedHashMap<String, Object> contactData = new LinkedHashMap<>();

        contactData.put("firstName", firstName.getValue());
        contactData.put("lastName", lastName.getValue());
        contactData.put("phoneNumber", phoneNumber.getValue());
        contactData.put("notes", notes.getValue());

        return contactData;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber.get();
    }

    public SimpleStringProperty phoneNumberProperty() {
        return phoneNumber;
    }

    public String getNotes() {
        return notes.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.set(phoneNumber);
    }

    public void setNotes(String notes) {
        this.notes.set(notes);
    }

    @Override
    public String toString() {
        return "Contact{" +
                "firstName=" + firstName +
                ", lastName=" + lastName +
                ", phoneNumber=" + phoneNumber +
                ", notes=" + notes +
                '}';
    }
}
