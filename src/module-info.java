module ContactsChallenge {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.xml;
    requires gson;
    requires java.sql;

    exports sample.datamodel;

    opens sample;
}